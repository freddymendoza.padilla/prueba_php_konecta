$(document).ready(function () {
	ListarProductos()
	validate()
	validarInput()
});

function validate() {
	$("#add_product").validate({

		rules: {

			code_prod: {
				required: true
			},
			peso_kg: {
				required: true
			},
			name_prod: {
				required: true
			},
			category_prod: {
				required: true,
				minlength: 3
			},
			stock_prod: {
				required: true
			}
			,
			price_prod: {
				required: true,
				minlength: 3
			}
		},
		messages: {
			code_prod: "Este campo es obligatorio",
			name_prod: "Este campo es obligatorio.",
			category_prod: "Este campo es obligatorio. (de 3 a 50 caracteres)",
			stock_prod: "Este campo es obligatorio.",
			price_prod: "Este campo es obligatorio.(mínimo 3 caracteres )",
		},
		errorElement: 'span',
	});

}

function validarInput() {
	jQuery('#code_prod').keyup(function () {
		this.value = this.value.replace(/[^0-9]/g, '');
	});
	jQuery('#peso_kg').keyup(function () {
		this.value = this.value.replace(/[^0-9]/g, '');
	});
	jQuery('#stock_prod').keyup(function () {
		this.value = this.value.replace(/[^0-9]/g, '');
	});
	jQuery('#price_prod').keyup(function () {
		this.value = this.value.replace(/[^0-9]/g, '');
	});
}
//Insertar Productos
$('#add_product').submit(function (event) {

	event.preventDefault();
	let info = $(this).serializeArray();
	//limpiar espacios
	for (var i = 0; i < info.length; i++) {

		if (info[i].name === "code_prod") {
			info.splice(0, 1, { name: 'code_prod', value: info[i].value.trim() });//reemplaza
		}
		else if (info[i].name === "name_prod") {
			info.splice(2, 1, { name: 'name_prod', value: info[i].value.trim() });//reemplaza
		}
		else if (info[i].name === "category_prod") {
			info.splice(3, 1, { name: 'category_prod', value: info[i].value.trim() });//reemplaza
		}

	}

	info.push({ name: 'opcn', value: 'insertar_info' });
	$.ajax({
		url: '../controlador/producto.php',
		type: 'POST',
		dataType: 'JSON',
		data: info,
	})
		.done(function ({ error, mensaje }) {
			if (!error) {
				$("#add_product")[0].reset();
				$('#addProductModal').modal('hide')
				ListarProductos()

			}
			mensajes(error, mensaje)
		})
		.fail(function ({ error, mensaje }) {
			mensajes(error, mensaje)
			console.log('error')
		})

});

//mensa o alerta
function mensajes(error, mensaje) {
	if (error) {

		Command: toastr["error"](mensaje)

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
	} else {

		Command: toastr["success"](mensaje)
		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

	}
}

//Listar Productos

function ListarProductos() {

	tabla = $('#tabla_productos').DataTable({
		ajax: {
			url: '../controlador/producto.php',
			type: 'POST',
			dataType: 'JSON',
			data: { opcn: 'Consultar_productos' },
			dataSrc: ''
		},
		columns: [
			{ data: 'id', title: 'ID' },
			{ data: 'code_prod', title: "Referencia" },
			{ data: 'name_prod', title: "Producto" },
			{ data: 'ctry_prod', title: "Categoria" },
			{ data: 'peso_prod', title: "Peso Kg" },
			{ data: 'stock_prod', title: "Stock" },
			{ data: 'date_creation', title: "Fecha de Creacion" },
			{ data: 'date_edition', title: "Fecha de Edicion" },
			{ data: 'price_prod', title: "Precio" },
			{ data: 'num_ventas', title: "Numero de ventas" },
			{
				data: null, title: "Opción",
				"render": function (data, type, row) {
					row.editar = '<button data-prod_id="' + row.id + '" data-stock= "' + row.stock_prod + '" type="button" class="btn btn-success btn-sm vender"  data-toggle="modal" data-target="#venderProductModal">Vender</button>-<button data-prod_id="' + row.id + '" type="button" class="editar btn btn-success btn-xs"  data-toggle="modal" data-target="#editProductModal"><i class="fa fa-edit" ></i></button>-<button data-prod_id="' + row.id + '" type="button" class="eliminar btn btn-danger btn-xs"  data-toggle="modal" data-target="#deleteProductModal"><i class="fa fa-edit" ></i></button>'

					return row.editar
				},
			},
		],
		destroy: true,
		"language": {
			"sProcessing": "Procesando...",
			"sLengthMenu": "Mostrar MENU registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningun dato disponible en esta tabla",
			"sInfo": "Mostrando registros del START al END de un total de TOTAL registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(de MAX registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "0‰3ltimo",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
		,
		dom: 'lBfrtip',
		pageLength: 5,
		buttons: [
			{
				extend: 'excelHtml5',
				text: '<i class="las la-file-excel">EXCEL</i>',
				filename: 'Reporte_Productos',
			},
			{
				extend: 'pdfHtml5',
				text: '<i class="las la-file-pdf">PDF</i>',
				//orientation: 'landscape',
				filename: 'Reporte_Productos',
				exportOptions: {
					// columns: [0,1,2,3,4,5,6,7,8],
				}
			},
			{ extend: 'print', text: '<i class="las la-print">IMPRIMIR</i>', filename: 'Reporte_Productos' }
		],
		columnDefs: [{
			width: "200px",
			targets: 10
		}
		]

	})
}

let id_produt;
//Consulta por Id en la DB
$('body').on('click', '.editar', function () {

	id_produt = $(this).data('prod_id');

	$.ajax({
		url: '../controlador/producto.php',
		type: 'POST',
		dataType: 'JSON',
		data: { opcn: 'Consult_Prod_id', id_produt },
	})
		.done(function (data) {

			for (var i = 0; i < data.length; i++) {

				$('#code_prodedit').val(data[i].code_prod)
				$('#peso_kgedit').val(data[i].peso_prod)
				$('#name_prodedit').val(data[i].name_prod)
				$('#category_prodedit').val(data[i].ctry_prod)
				$('#stock_prodedit').val(data[i].stock_prod)
				$('#price_prodedit').val(data[i].price_prod)
			}

		})

		.fail(function () {
			console.log("error");
		})
})

// Editar producto
$('#edit_product').submit(function (e) {
	e.preventDefault();

	var datos = $(this).serializeArray();
	datos.push({ name: 'opcn', value: 'Edit_produc' })
	datos.push({ name: 'id', value: id_produt })

	$.ajax({
		url: '../controlador/producto.php',
		type: 'POST',
		dataType: 'JSON',
		data: datos,
	})
		.done(function ({ error, mensaje }) {
			if (!error) {
				$('#editProductModal').modal('hide')
				ListarProductos()
			}
			mensajes(error, mensaje)

		})
		.fail(function () {
			console.log("error");
		})

});

//eliminar producto
$('body').on('click', '.eliminar', function (event) {
	event.preventDefault()
	let id = $(this).data('prod_id');
	$('#delete_id').val(id)
	//console.log(id);
});

$('#delete_product').submit(function (event) {
	event.preventDefault();
	data = $(this).serializeArray();
	data.push({ name: 'opcn', value: 'Delet_produc' })

	$.ajax({
		url: '../controlador/producto.php',
		type: 'POST',
		dataType: 'JSON',
		data: data,
	})
		.done(function (data) {
			console.log(data);
			$('#deleteProductModal').modal('hide')
			ListarProductos()
		})
		.fail(function () {
			console.log("error");
		})

});

//vender producto
$('body').on('click', '.vender', function (event) {
	event.preventDefault()
	$('#stock_vender').val($(this).data('stock'))
	$('#id_producto').val($(this).data('prod_id'))
});

$("#vender_producto").submit(function (e) {
	e.preventDefault();
	let datos = $(this).serializeArray();
	datos.push({ name: 'opcn', value: 'vender_producto' });

	if ($('#num_cantidad').val() <= 0) {
		$error = true;
		$mensaje = 'Error, La cantidad debe ser mayor a cero';
		mensajes($error, $mensaje)

	} else {
		if ($('#num_cantidad').val() <= $('#stock_vender').val()) {


			$.ajax({
				url: '../controlador/producto.php',
				type: 'POST',
				dataType: 'JSON',
				data: datos,
			})
				.done(function ({ error, mensaje }) {

					$("#vender_producto")[0].reset();
					$("#venderProductModal").modal("hide")
					ListarProductos()
					mensajes(error, mensaje)
				})
				.fail(function ({ error, mensaje }) {
					mensajes(error, mensaje)
				});
		} else {

			$error = true;
			$mensaje = 'Error, La cantidad supera el stock del producto, debe ser igual o menor al stock';
			mensajes($error, $mensaje)

		}

	}


});




