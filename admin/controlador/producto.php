<?php

$metodo = $_SERVER['REQUEST_METHOD'];
switch ($metodo) {
    case 'POST':
        include_once('../modelo/producto.php');
        switch ($_POST['opcn']) {
            case 'insertar_info':
                $info = Product::mdlInsert($_POST);
                echo json_encode($info);
                break;
            case 'Consultar_productos':
                $info = Product::mdlListar();
                echo json_encode($info);
                break;
            case 'Consult_Prod_id':
                $info = Product::consProdId($_POST['id_produt']);
                echo json_encode($info);
                break;
            case 'Edit_produc':
                $info = Product::editProducto($_POST);
                echo json_encode($info);
                break;
            case 'Delet_produc':
                $info = Product::deletProducto($_POST['delete_id']);
                echo json_encode($info);
                break;
            case 'vender_producto':
                $info = Product::vender_producto($_POST);
                echo json_encode($info);
                break;
        }
        break;
}
