<div id="venderProductModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="vender_producto">
                <div class="modal-header">
                    <h4 class="modal-title text-center	">Vender Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class='col-md-6'>
                            <div class="form-group">
                                <label>Stock</label>
                                <input type="text" name="stock_vender" id="stock_vender" class="form-control" readonly>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <label>Cantidad</label>
                                <input type="number" name="num_cantidad" id="num_cantidad" class="form-control" placeholder="" required>

                            </div>
                        </div>
                        <input type="number" name="id_producto" id="id_producto" class="form-control" style="display: none;">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                    <input type="submit" class="btn btn-success" value="Vender">
                </div>
            </form>
        </div>
    </div>
</div>