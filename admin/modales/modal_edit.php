<div id="editProductModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="edit_product" id="edit_product">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Producto</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Código</label>
							<input type="text" name="code_prod"  id="code_prodedit" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Peso</label>
							<input type="number" name="peso_kg"  id="peso_kgedit" class="form-control" placeholder="KG" required>
							
						</div>
						<div class="form-group">
							<label>Producto</label>
							<input type="text" name="name_prod" id="name_prodedit" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Categoría</label>
							<input type="text" name="category_prod" id="category_prodedit" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Stock</label>
							<input type="number" name="stock_prod" id="stock_prodedit" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Precio</label>
							<input type="number" name="price_prod" id="price_prodedit" class="form-control" required>
						</div>					
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Guardar_datos">
					</div>
				</form>
			</div>
		</div>
	</div>		