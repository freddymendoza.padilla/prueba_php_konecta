<?php
include_once('../../config/init_db.php');

DB::$error_handler = false;
DB::$throw_exception_on_error = true;

class Product
{
	public static function validar_cod($code_prod)
	{
		try {
			$validar = DB::query("SELECT * FROM tabla_producto WHERE code_prod = $code_prod");
			if (!empty($validar)) {
				$data['error'] = true;
				$data['mensaje'] = 'Codigo ya existe';
			} else {
				$data['error'] = false;
				$data['mensaje'] = 'Seguir';
			}
		} catch (MeekroDBException $e) {
			$data['error'] = true;
			$data['mensaje'] = 'Error, no se logro guardar la información';
		}
		return $data;
		DB::disconnect();
	}


	public static function mdlInsert($info)
	{
		extract($info);

		try {
			$validar = Product::validar_cod($code_prod);
		

			$insert = DB::query(" INSERT INTO tabla_producto(
													code_prod,
													name_prod,
													ctry_prod,
													price_prod,
													peso_prod,
													stock_prod,                          date_creation                  
                                                     )
                                                     VALUES(
                                                     '$code_prod',
                                                     '$name_prod',
                                                     '$category_prod',
                                                      $price_prod,
                                                      $peso_kg,
                                                      $stock_prod,
													  NOW()
                                                            )");
			$data['error'] = false;
			$data['mensaje'] = 'Producto creado correctamente';
		} catch (MeekroDBException $e) {
			$data['error'] = true;
			$data['mensaje'] = 'Error, no se logro guardar el producto, Código/Referencia Duplicado';
		}
		return $data;
		DB::disconnect();
	}

	public static function mdlListar()
	{

		try {
			$listar = db::query("SELECT * FROM tabla_producto");
		} catch (MeekroDBException $e) {
			echo "Error:" . $e->getMessage() . "<br>\n";
			echo "SQL Query:" . $e->getMessage() . "<br>\n";
		}
		return $listar;
		DB::disconnect();
	}

	public static function consProdId($id_produt)
	{

		try {
			$listar = db::query("SELECT * FROM tabla_producto where id = $id_produt");
		} catch (MeekroDBException $e) {
			echo "Error:" . $e->getMessage() . "<br>\n";
			echo "SQL Query:" . $e->getMessage() . "<br>\n";
		}
		return $listar;
		DB::disconnect();
	}

	public static function editProducto($p)
	{
		extract($p);
		try {

			$codigoactual = DB::queryFirstRow("SELECT * FROM tabla_producto WHERE id = $id");
			if ($codigoactual['code_prod'] != $code_prod) {
				$validar = Product::validar_cod($code_prod);
				if ($validar['error']) {
					return $validar;
				}
			}
			DB::query(" UPDATE
										    tabla_producto
										SET
										    code_prod  = '{$code_prod}',
										    name_prod  = '{$name_prod}',
										    ctry_prod  = '{$category_prod}',
										    price_prod = {$price_prod},
										    peso_prod  =   $peso_kg,
										    stock_prod = {$stock_prod},
										    date_edition = NOW()
										  
										WHERE
										    id = $id ");
			$data['error'] = false;
			$data['mensaje'] = 'Producto editado correctamente';
		} catch (MeekroDBException $e) {
			$data['error'] = true;
			$data['mensaje'] = 'Error al editar producto';
		}
		return $data;
		DB::disconnect();
	}

	public static function deletProducto($delete_id)
	{

		try {

			$deletprod = DB::query("DELETE FROM tabla_producto WHERE  id = $delete_id");
		
			if ($deletprod) {
				$resultado['error'] = false;
				$resultado['mensaje'] = "Producto eliminado";
			}
		} catch (MeekroDBException $e) {
		
			$resultado['error'] = true;
			$resultado['mensaje'] = "error al eliminar producto";
		}
		return $resultado;
		DB::disconnect();
	}

	public static function vender_producto($p)
	{

		extract($p);
		$mas_vendido = 0;
		$listar = db::queryFirstRow("SELECT stock_prod, num_ventas FROM tabla_producto where id = $id_producto;");
		$stock_prod = $listar['stock_prod'];
		$num_ventas = $listar['num_ventas'];
		
		$mas_vendido = is_null($mas_vendido) ? $num_cantidad : $num_ventas + $num_cantidad;

		$resultado = array();
		if ($stock_prod > 0) {
			$date_vent     = date("Y-m-d") . " " . date("H") . ":" . date("i:s");
			$res = DB::query("UPDATE tabla_producto 
								SET 
								stock_prod = $stock_prod - $num_cantidad, 
								num_ventas = $mas_vendido,
								date_vent = '$date_vent' 
								where id = $id_producto");
		
			if ($res) {
				$resultado['error'] = false;
				$resultado['mensaje'] = "Producto vendido";
			}
		} else {
			$resultado['error'] = true;
			$resultado['mensaje'] = "Sin stock";
		}

		return $resultado;
		DB::disconnect();
	}


}
