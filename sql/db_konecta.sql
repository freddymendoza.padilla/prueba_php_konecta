-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2022 at 08:04 AM
-- Server version: 5.7.33
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_konecta`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabla_producto`
--

CREATE TABLE `tabla_producto` (
  `id` int(10) NOT NULL,
  `code_prod` varchar(50) NOT NULL,
  `name_prod` varchar(50) NOT NULL,
  `ctry_prod` varchar(50) NOT NULL,
  `price_prod` int(11) NOT NULL,
  `peso_prod` int(11) NOT NULL,
  `stock_prod` int(10) NOT NULL,
  `date_creation` date NOT NULL,
  `date_edition` datetime DEFAULT NULL,
  `date_vent` datetime DEFAULT NULL,
  `num_ventas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tabla_producto`
--

INSERT INTO `tabla_producto` (`id`, `code_prod`, `name_prod`, `ctry_prod`, `price_prod`, `peso_prod`, `stock_prod`, `date_creation`, `date_edition`, `date_vent`, `num_ventas`) VALUES
(1, '0002', 'Pan', 'panaderia', 1000, 12, 22, '2022-03-29', '2022-03-29 01:01:39', '2022-03-29 02:48:20', 6),
(2, '0003', 'Pan', 'panadería', 2333, 25, 20, '2022-03-29', '2022-03-29 03:00:10', '2022-03-29 02:58:24', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabla_producto`
--
ALTER TABLE `tabla_producto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `libro` (`code_prod`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabla_producto`
--
ALTER TABLE `tabla_producto`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
